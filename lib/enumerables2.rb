require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  if arr == []
    return 0
  end
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.each do |str|
    if str.include?(substring) == false
      return false
    end
  end
  true
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  arr_chars = string.chars
  arr_chars.delete(" ")
  arr_chars.reject! { |ch| arr_chars.count(ch) == 1 }
  arr_chars.uniq.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  stripped = string.delete(".!?&:,")
  words = stripped.split(" ")
  words.sort_by! { |wrd| wrd.length }
  words[-2..-1].reverse
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = "abcdefghijklmnopqrstuvwxyz"
  alphabet.delete!(string)
  alphabet.chars
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).to_a.select do |yr|
    stringified = yr.to_s
    stringified.chars.uniq == stringified.chars
  end
end

def not_repeat_year?(year)

end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  (songs.select {|song| no_repeats?(song, songs) }).uniq
end

def no_repeats?(song_name, songs)
  songs.each_index do |ind|
    if songs[ind] == song_name && songs[ind + 1] == song_name
      return false
    end
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  words = string.delete(".,?!&").split(" ")
  c_words = words.select { |wrd| wrd.include?("c") }
  c_words.sort_by { |wrd| c_distance(wrd) }[0]
end

def c_distance(word)
  word.reverse.index("c")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  result = []
  beginning = nil
  arr.each_with_index do |num, ind|
    if arr[ind] != arr[ind + 1]
      if beginning
        result << [beginning, ind]
        beginning = nil
      end
    elsif arr[ind] == arr[ind + 1]
      if beginning == nil
        beginning = ind
      end
    end
  end
  result
end
